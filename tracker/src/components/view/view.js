import './view.css';

function View(props) {
  return (
    <div className="View">
     {props.children}
    </div>
  );
}

export default View;
